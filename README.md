# Kotlin MUI Showcase

### What this fork is about

This repository is a fork of the [Kotlin MUI Showcase](https://github.com/karakum-team/kotlin-mui-showcase), adding [Vite support](https://gitlab.com/opensavvy/automation/kotlin-vite).

### Demo Stand of Kotlin/JS Material-UI

This project uses official Kotlin MUI Wrappers:

- [MUI](https://github.com/JetBrains/kotlin-wrappers/tree/master/kotlin-mui)
- [MUI Icons](https://github.com/JetBrains/kotlin-wrappers/tree/master/kotlin-mui-icons)

#### App Snapshots

> ![Accordion](.doc/accordion.jpg)
> ![Alert](.doc/alert.jpg)
> ![Dialog](.doc/dialog.jpg)
> ![Image List](.doc/image-list.jpg)
> ![Menu](.doc/menu.jpg)
> ![Table](.doc/table.jpg)

----

## Useful commands

### Clean

```sh
./gradlew clean
./gradlew --stop
```

### Build

```sh
./gradlew build
```

### Run

```sh
./gradlew jsRun -t
```

### Gradle Wrapper update

```sh
./gradlew wrapper
```
